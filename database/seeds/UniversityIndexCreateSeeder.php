<?php

use Illuminate\Database\Seeder;

class UniversityIndexCreateSeeder extends Seeder {

    public function run(\Elasticsearch\Client $elastic)
    {
        $params = [
            'index' => 'university',
        ];

        if ($elastic->indices()->exists($params)){
            $elastic->indices()->delete($params);
        }

        $elastic->indices()->create($params);
    }
}