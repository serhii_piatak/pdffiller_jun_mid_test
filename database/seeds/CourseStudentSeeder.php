<?php

use App\Models\Course;
use App\Models\Student;
use Illuminate\Database\Seeder;

class CourseStudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $students = Student::all();

        foreach($students as $student) {
            // get random courses in quantity 0..20
            $courses = Course::inRandomOrder()->take(rand(0,20))->get();
            $courseIds = $courses->isNotEmpty() ? $courses->pluck('id') : [];

            $student->courses()->attach($courseIds);
        }
    }
}
