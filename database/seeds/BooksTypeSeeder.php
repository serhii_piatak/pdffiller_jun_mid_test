<?php

use App\Repositories\ElasticBookRepository;
use Illuminate\Database\Seeder;

class BooksTypeSeeder extends Seeder {

    /**
     * @var \App\Models\Book
     */
    private $book;

    public function __construct(ElasticBookRepository $book)
    {
        $this->book = $book;
    }

    public function run(Faker\Generator $faker)
    {
        // set mapping
        $this->book->mapping();

        for($i = 0; $i < 1500; $i++) {
            $this->book->addDocument([
                'title'       => $faker->realText(50),
                'description' => $faker->realText(500),
                'created_at'  => $faker->date('Y'),
                'pages'       => $faker->numberBetween(55, 555),
            ]);
            $this->command->getOutput()->write('.');
        }
        $this->command->info('Books seeded');
    }
}