<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // part #1
        $this->call(CoursesTableSeeder::class);
        $this->call(StudentsTableSeeder::class);
        $this->call(CourseStudentSeeder::class);

        // part #2
        $this->call(UniversityIndexCreateSeeder::class);
        $this->call(BooksTypeSeeder::class);
    }
}
