<?php

namespace App\Repositories\Interfaces;


use App\Models\Interfaces\ElasticType;

interface ElasticRepository {

    /**
     * Create mapping
     *
     * @return mixed
     */
    public function mapping();

    /**
     * Add document to specific index.type
     *
     * @param array $properties
     * @return mixed
     */
    public function addDocument(array $properties);

    /**
     * Update document in specific index.type
     *
     * @param int $id
     * @return mixed
     */
    public function updateDocument($id, $properties);

    /**
     * Remove document from specific index.type
     *
     * @param $id
     * @return mixed
     */
    public function deleteDocument($id);

    /**
     * Search documents
     *
     * @return mixed
     */
    public function search(array $params);

    /**
     * Find document by Id
     *
     * @param $id
     * @throws \Exception Not found document with this id
     */
    public function findOrFail($id);
}