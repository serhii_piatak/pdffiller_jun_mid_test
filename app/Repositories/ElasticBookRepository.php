<?php

namespace App\Repositories;


use App\Models\Book;
use Elasticsearch\Client;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ElasticBookRepository extends ElasticBaseRepository {

    /**
     * @var Client
     */
    private $elastic;
    /**
     * @var Book
     */
    private $book;

    public function __construct(Client $elastic, Book $book)
    {
        $this->elastic = $elastic;
        $this->book = $book;
    }

    public function mapping()
    {
        $params = [
            'index' => $this->book->getIndex(),
            'type'  => $this->book->getType(),
            'body'  => [
                $this->book->getType() => [
                    '_source'    => [
                        'enabled' => true
                    ],
                    'properties' => [
                        'title'       => [
                            'type' => 'string',
                        ],
                        'description' => [
                            'type' => 'string',
                        ],
                        'created_at'  => [
                            'type'   => 'date',
                            'format' => 'YYYY',
                        ],
                        'pages'       => [
                            'type' => 'integer',
                        ],
                    ]
                ]
            ]
        ];
        $this->elastic->indices()->putMapping($params);
    }

    public function addDocument(array $properties)
    {
        $this->elastic->index([
            'index' => $this->book->getIndex(),
            'type'  => $this->book->getType(),
            'body'  => $properties,
        ]);
    }

    public function updateDocument($id, $properties)
    {
        $params = [
            'index' => $this->book->getIndex(),
            'type'  => $this->book->getType(),
            'id'    => $id,
        ];

        if (!$this->elastic->exists($params)) {
            throw new NotFoundHttpException('Book not found');
        }

        $this->elastic->update($params + [
            'body'  => ['doc' => $properties],
        ]);
    }

    public function deleteDocument($id)
    {
        $params = [
            'index' => $this->book->getIndex(),
            'type'  => $this->book->getType(),
            'id'    => $id,
        ];

        if (!$this->elastic->exists($params)) {
            throw new NotFoundHttpException('Book not found');
        }

        $this->elastic->delete($params);
    }

    public function search(array $fields)
    {
        if (empty($fields)) {
            $query = [
                'query' => ['match_all' => new \stdClass()]
            ];
        } else {
            $matches = [];
            foreach($fields as $field => $value) {
                $matches[] = ['match' => [$field => $value ?? '']];
            }
            $query = [
                'query' => [
                    'bool' => [
                        'should' => $matches,
                    ],
                ]
            ];
        }

        $params = [
            'index' => $this->book->getIndex(),
            'type'  => $this->book->getType(),
            'size'  => 50,
            'body'  => $query,
        ];

        return $this->elastic->search($params);
    }

    /**
     * @param $id
     * @return array
     * @throws \Exception
     */
    public function findOrFail($id)
    {
        $book = $this->elastic->get([
            'index' => $this->book->getIndex(),
            'type'  => $this->book->getType(),
            'id'    => $id,
        ]);

        if ( ! $book) {
            throw new \Exception('Book does not found');
        }

        return $book;
    }
}