<?php

namespace App\Models\Interfaces;


interface ElasticType {
    /**
     * Get index
     *
     * @return string
     */
    public function getIndex(): string;

    /**
     * Get type
     *
     * @return string
     */
    public function getType(): string;
}