<?php

namespace App\Models;


class Book extends ElasticBaseType {

    protected $index = 'university';
    protected $type = 'book';
}