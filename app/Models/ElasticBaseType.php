<?php

namespace App\Models;


use App\Models\Interfaces\ElasticType;

abstract class ElasticBaseType implements ElasticType {

    protected $index;
    protected $type;

    public function getIndex(): string
    {
        return $this->index;
    }

    public function getType(): string
    {
        return $this->type;
    }


}