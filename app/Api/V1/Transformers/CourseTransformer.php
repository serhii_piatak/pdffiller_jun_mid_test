<?php

namespace App\Api\V1\Transformers;

use App\Models\Course;
use League\Fractal\TransformerAbstract;

class CourseTransformer extends TransformerAbstract {

    /**
     * @param Course $course
     *
     * @return array
     */
	public function transform(Course $course)
	{
		return [
		    'title' => $course->title,
		];
	}
}