<?php

namespace App\Api\V1\Transformers;

use App\Models\Student;
use League\Fractal\TransformerAbstract;

class StudentTransformer extends TransformerAbstract {

    /**
     * @param Student $student
     *
     * @return array
     */
	public function transform(Student $student)
	{
		return [
		    'name' => $student->name,
		];
	}
}