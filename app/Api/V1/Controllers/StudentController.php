<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Transformers\StudentTransformer;
use App\Http\Requests\StudentRequest;
use App\Models\Course;
use App\Models\Student;
use Dingo\Api\Http\Request;

class StudentController extends ApiBaseController {

    /**
     * @var Student
     */
    private $student;

    /**
     * @param Student $student
     */
    public function __construct(Student $student)
    {
        $this->student = $student;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = $this->student->paginate();

        return $this->response->paginator($students, new StudentTransformer());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StudentRequest $studentRequest
     * @return \Illuminate\Http\Response
     */
    public function store(StudentRequest $studentRequest)
    {
        $this->student->create($studentRequest->all());

        return $this->response->created();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param                $id
     * @param StudentRequest $studentRequest
     * @return \Illuminate\Http\Response
     */
    public function update($id, StudentRequest $studentRequest)
    {
        $student = $this->getStudent($id);
        $student->update($studentRequest->all());

        return $this->response->item(null, null)->setStatusCode(200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param         $id
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $student = $this->getStudent($id);

        $student->delete();

        return $this->response->item(null, null)->setStatusCode(204);
    }

    /**
     * @param $student
     * @param $course
     * @return \Dingo\Api\Http\Response
     */
    public function attachCourse($student, $course)
    {
        $student = $this->getStudent($student);
        $course = $this->getCourse($course);

        $student->courses()->attach([$course->id]);

        return $this->response->created();
    }

    /**
     * @param $student
     * @param $course
     * @return \Dingo\Api\Http\Response
     */
    public function detachCourse($student, $course)
    {
        $student = $this->getStudent($student);
        $course = $this->getCourse($course);

        $student->courses()->detach([$course->id]);

        return $this->response->item(null, null)->setStatusCode(204);
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Collection|Student
     */
    private function getStudent($id)
    {
        try {
            $student = $this->student->findOrFail($id);
        } catch(\Exception $e) {
            $this->response->errorNotFound('Student does not found');
        }

        return $student;
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Collection|Course
     */
    private function getCourse($id)
    {
        try {
            $course = Course::findOrFail($id);
        } catch(\Exception $e) {
            $this->response->errorNotFound('Course does not found');
        }

        return $course;
    }
}
