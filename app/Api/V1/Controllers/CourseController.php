<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Transformers\CourseTransformer;
use App\Http\Requests\CourseRequest;
use App\Models\Course;
use Dingo\Api\Http\Request;

class CourseController extends ApiBaseController {

    /**
     * @var Course
     */
    private $course;

    /**
     * @param Course $course
     */
    public function __construct(Course $course)
    {
        $this->course = $course;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = $this->course->paginate();

        return $this->response->paginator($courses, new CourseTransformer());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CourseRequest $courseRequest
     * @return \Illuminate\Http\Response
     */
    public function store(CourseRequest $courseRequest)
    {
        $this->course->create($courseRequest->all());

        return $this->response->created();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param                $id
     * @param CourseRequest $courseRequest
     * @return \Illuminate\Http\Response
     */
    public function update($id, CourseRequest $courseRequest)
    {
        $course = $this->getCourse($id);
        $course->update($courseRequest->all());

        return $this->response->item(null, null)->setStatusCode(200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param         $id
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $course = $this->getCourse($id);

        $course->delete();

        return $this->response->item(null, null)->setStatusCode(204);
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    private function getCourse($id)
    {
        try {
            $course = $this->course->findOrFail($id);
        } catch(\Exception $e) {
            $this->response->errorNotFound();
        }

        return $course;
    }
}
