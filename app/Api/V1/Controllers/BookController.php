<?php

namespace App\Api\V1\Controllers;


use App\Http\Requests\BookRequest;
use App\Repositories\ElasticBookRepository;
use Dingo\Api\Contract\Http\Request;

class BookController extends ApiBaseController {

    /**
     * @var ElasticBookRepository
     */
    private $book;

    /**
     * BookController constructor.
     *
     * @param ElasticBookRepository $book
     */
    public function __construct(ElasticBookRepository $book)
    {
        $this->book = $book;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->book->search($request->all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param BookRequest $bookRequest
     * @return \Illuminate\Http\Response
     */
    public function store(BookRequest $bookRequest)
    {
        $this->book->addDocument($bookRequest->all());

        return $this->response->created();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param                $id
     * @param BookRequest $bookRequest
     * @return \Illuminate\Http\Response
     */
    public function update($id, BookRequest $bookRequest)
    {
        $this->book->updateDocument($id, $bookRequest->all());

        return $this->response->item(null, null)->setStatusCode(200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param         $id
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $this->book->deleteDocument($id);

        return $this->response->item(null, null)->setStatusCode(204);
    }
}