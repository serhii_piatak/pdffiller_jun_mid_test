<?php

namespace App\Api\V1\Controllers;

use Dingo\Api\Routing\Helpers;
use Illuminate\Routing\Controller;

abstract class ApiBaseController extends Controller {
    use Helpers;
}