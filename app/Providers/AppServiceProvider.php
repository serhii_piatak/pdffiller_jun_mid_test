<?php

namespace App\Providers;

use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Validation rules
        Validator::extend('unsigned_int', function ($attribute, $value, $parameters, $validator) {
            return preg_match('/^([1-9]\d*)?$/', $value) === 1;
        });

        $this->bindSearchClient();
    }

    /**
     * Initiate Elastic Client
     */
    private function bindSearchClient()
    {
        $this->app->bind(Client::class, function ($app) {
            return ClientBuilder::create()
                ->setHosts(config('services.elastic.hosts'))
                ->build();
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
