#!/usr/bin/env bash

sudo apt-get update

# ELASTICSEARCH
echo ">> Installing Elastic GPG Key"
wget -O - http://packages.elasticsearch.org/GPG-KEY-elasticsearch | apt-key add -

echo ">> Adding deb package"
wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-5.6.3.deb

echo ">> Updating apt"
sudo add-apt-repository ppa:webupd8team/java
sudo apt-get update

echo ">> Pre-agreeing to Oracle License"
echo debconf shared/accepted-oracle-license-v1-1 select true | \
  sudo debconf-set-selections
echo debconf shared/accepted-oracle-license-v1-1 seen true | \
  sudo debconf-set-selections

echo ">> Installing Java and Elastic Search"
#sudo apt-get install -y oracle-java9-installer
sudo apt-get install -y default-jdk
sudo dpkg -i elasticsearch-5.6.3.deb

echo ">> Java Installed"
echo ">> Elastic Search Installed"

echo ">> Scheduling Elasticsearch"
sudo systemctl enable elasticsearch.service

echo ">> Starting Elasticsearch"
sudo /etc/init.d/elasticsearch start

echo ">> Running on port 9200. Make sure to add a firewall rule if you need external access."

cd /app
echo ">> Composer update"
sudo composer update

echo ">> copy .env"
cp -b .env.example .env

echo ">> genrate key"
php artisan key:generate

echo ">> Migrate"
php artisan migrate
echo ">> Seed"
php artisan db:seed