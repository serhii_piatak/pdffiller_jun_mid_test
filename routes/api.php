<?php

/** @var Dingo\Api\Routing\Router $api */
$api = app('Dingo\Api\Routing\Router');

$api->version('v1', [
    'middleware' => [
//        'auth:api',
    ],
    'namespace'  => '\App\Api\V1\Controllers',
], function (Dingo\Api\Routing\Router $api) {

    $api->resources([
        'student' => 'StudentController',
        'course'  => 'CourseController',
        'book'    => 'BookController',
    ]);

    $api->post('student/{student}/course/{course}', 'StudentController@attachCourse');
    $api->delete('student/{student}/course/{course}', 'StudentController@detachCourse');
});