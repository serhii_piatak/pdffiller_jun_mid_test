# Install
1. ```composer install```
2. Specify VM ip in Homestead.yaml
3. ```vagrant up```

# Notice
There postman collection with environment variables:
1.  ```postman/collection.json```
2.  ```postman/environment.json```